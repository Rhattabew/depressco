﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Control : MonoBehaviour
{
    public Rigidbody2D rb;
    public GameObject player;
    public GameObject[] enemy;
  
    // Start is called before the first frame update
    void Start()
    {
       rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }
    void Movement()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddTorque(.5f);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddTorque(-.5f);
        }
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.up * 3);
        }
        if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(transform.up * -2);
        }
    }



    }
